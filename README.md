# Urban Magnetometry Analysis Software

[![License MIT](https://img.shields.io/badge/License-MIT-blue.svg)](http://opensource.org/licenses/MIT)
[![PyPI version](https://badge.fury.io/py/nuri.svg)](https://badge.fury.io/py/nuri)

The full documentation of the program can be accessed [here](https://citymag.gitlab.io/nuri/).

## License

NURI is an open-source software available and free to use under the [MIT License](https://gitlab.com/citymag/analysis/nuri/blob/master/LICENSE).

Copyright (c) 2019, Urban Magnetometry Group.
