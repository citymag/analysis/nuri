#!/bin/bash
git clone https://gitlab.com/citymag/analysis/mlpy.git mlpy
rm mlpy/mlpy/gsl/gsl.c mlpy/mlpy/liblinear/liblinear.c mlpy/mlpy/libsvm/libsvm.c mlpy/mlpy/adatron/adatron.c
apt-get install libgsl-dev
cd mlpy && python setup.py build_ext --include-dirs=/usr/include --rpath=/usr/lib && python setup.py install --single-version-externally-managed --root=/
rm -rf mlpy
pip install gwpy obspy matplotlib==3.1.3 scipy==1.5.0
ln -s /content/nuri/nuri $(python -c "import pip; print(pip.__path__[0].rstrip('/pip'))")/
